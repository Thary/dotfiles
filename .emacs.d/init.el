;;; init.el --- Summary
;;; Commentary:
;;; Settings for GNU EMACS.
;;; Code:
(provide 'init)

(global-display-line-numbers-mode) ;; Выводить номер строки

(fset 'yes-or-no-p 'y-or-n-p) ;; Упрощение подтверждения действия

(setq-default cursor-type 'bar) ;; Установка курсора

;; Заставляем use-package устанавливать пакеты
(require 'package)

(setq package-archives '(("melpa" . "https://melpa.org/packages/")
			 ("nongnu" . "https://elpa.nongnu.org/nongnu/")
                         ("elpa"  . "https://elpa.gnu.org/packages/"))) ;; Подключение архивов с пакетами

(package-initialize)
(unless (package-installed-p 'use-package)
  (package-install 'use-package))

(require 'use-package-ensure)
(setq use-package-always-ensure t)

;; Пакеты
(use-package nord-theme) ;; Тема Nord
(use-package yasnippet) ;; Набор снипетов
(use-package web-mode) ;; Режим для веб-разработки
(use-package ibuffer) ;; Красивая переключалка буферов
(use-package elpher) ;; Браузер для Gemeni и Gopher
(use-package language-detection) ;; Определения ЯПа
(use-package all-the-icons) ;; Красивые иконки
(use-package doom-modeline) ;; Строка состояние
(use-package flycheck) ;; Дебагер

;; Настройка web-mode
(add-to-list 'auto-mode-alist '("\\.html\\'" . web-mode)) ;; Автоматически вкючать web-mode в .html
(setq web-mode-enable-current-element-highlight t)

(auto-fill-mode 0) ;; Выключить auto-fill-mode

(global-flycheck-mode) ;; Включить дебагер

;; Включение IDO
(require 'ido)
(ido-mode t)

(require 'tramp) ;; Включить удаленное редактирование документов

;; Включение темы
(load-theme 'nord t)

;; Строка состояния
(setq
      doom-modeline-buffer-encoding         nil
      doom-modeline-buffer-file-name-style  'truncate-from-project)
      (doom-modeline-mode 1)

(set-face-attribute 'mode-line nil
      :background "gray10"
      :foreground "#FCE8C3")
(set-face-attribute 'mode-line-inactive nil
      :background "gray5"
      :foreground "#FCE8C3")

;; Zooming
(global-set-key (kbd "C-+") 'text-scale-increase) ;; Увеличить размер шрифта
(global-set-key (kbd "C--") 'text-scale-decrease) ;; Уменьшить размер шрифта

;; Работа с буферами
(global-set-key (kbd "C-<next>") 'next-buffer) ;; Перейти в следующий буфер 
(global-set-key (kbd "C-<prior>") 'previous-buffer) ;; Перейти в предыдущий буфер
(global-set-key (kbd "C-a") 'mark-whole-buffer) ;; Выделить всё
(global-set-key (kbd "C-o") 'dired) ;; Запустить dired-mode
(global-set-key (kbd "C-r") 'revert-buffer) ;; Обновить буфер

;; Работа с файлом
(global-set-key (kbd "C-s") 'save-buffer)  ;; Сохранить файл
(global-set-key (kbd "C-S-s") 'write-file) ;; Сохранить файл под другим именем

(setq-default
 create-lockfiles 0 ;; Отключить блокирование файлов
 inhibit-splash-screen t ;; Не показывать заставку
 inhibit-startup-message t ;; Не показывeать сообщение при запуске
 initial-major-mode 'fundamental-mode ;; Использовать fundamental-mode для новых буферов
 initial-scratch-message "" ;; Сообщение в новых пустых буферах
 make-backup-files nil ;; Не создавать резервные копии редактируемых файлов
 truncate-lines 1 ;; Переносить длинные строки
 user-email-address "thary@riseup.net" ;; Ваш email
 user-full-name "Thary") ;; Ваше полное имя

;; Разрешить редактирование файлов в ~/.emacs.d
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("37768a79b479684b0756dec7c0fc7652082910c37d8863c35b702db3f16000f8" default))
 '(package-selected-packages
   '(markdown-mode doom-modeline elpher yasnippet-snippets evil-mode use-package nord-theme))
 '(safe-local-variable-val ues)
 '(safe-local-variable-values '((git-commit-major-mode . git-commit-elisp-text-mode)))
 '(warning-suppress-types '((use-package))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(cua-mode 0) ;; Отключение Ctrl+C, Ctrl-V и т. д.

;; Скрыть лишние элементы
(menu-bar-mode 1) ;; Скрыть/показать меню
(scroll-bar-mode 0) ;; Скрыть полосы прокрутки
(tool-bar-mode 0) ;; Скрыть панель кнопок
(tooltip-mode 0) ;; Не выводить подсказки
(window-divider-mode 0) ;; Не показывать границу окон

;; Сохранять состояние
(defvar desktop-save t "Save desktop settings without any questions")
(setq desktop-save t)
(desktop-save-mode 1)

;; Заставить EWW работать с протоколами Gemeni и Gopher
(advice-add 'eww-browse-url :around 'elpher:eww-browse-url)
(defun elpher:eww-browse-url (original url &optional new-window)
  "Handle gemini links."
  (cond ((string-match-p "\\`\\(gemini\\|gopher\\)://" url)
	 (require 'elpher)
	 (elpher-go url))
	(t (funcall original url new-window))))

;; Подсветка блоков кода в EWW
(require 'cl-lib)
(defun eww-tag-pre (dom)
  (let ((shr-folding-mode 'none)
        (shr-current-font 'default))
    (shr-ensure-newline)
    (insert (eww-fontify-pre dom))
    (shr-ensure-newline)))
(defun eww-fontify-pre (dom)
  (with-temp-buffer
    (shr-generic dom)
    (let ((mode (eww-buffer-auto-detect-mode)))
      (when mode
        (eww-fontify-buffer mode)))
    (buffer-string)))
(defun eww-fontify-buffer (mode)
  (delay-mode-hooks (funcall mode))
  (font-lock-default-function mode)
  (font-lock-default-fontify-region (point-min)
                                    (point-max)
                                    nil))
(defun eww-buffer-auto-detect-mode ()
  (let* ((map '((ada ada-mode)
                (awk awk-mode)
                (c c-mode)
                (cpp c++-mode)
                (clojure clojure-mode lisp-mode)
                (csharp csharp-mode java-mode)
                (css css-mode)
                (dart dart-mode)
                (delphi delphi-mode)
                (emacslisp emacs-lisp-mode)
                (erlang erlang-mode)
                (fortran fortran-mode)
                (fsharp fsharp-mode)
                (go go-mode)
                (groovy groovy-mode)
                (haskell haskell-mode)
                (html html-mode)
                (java java-mode)
                (javascript javascript-mode)
                (json json-mode javascript-mode)
                (latex latex-mode)
                (lisp lisp-mode)
                (lua lua-mode)
                (matlab matlab-mode octave-mode)
                (objc objc-mode c-mode)
                (perl perl-mode)
                (php php-mode)
                (prolog prolog-mode)
                (python python-mode)
                (r r-mode)
                (ruby ruby-mode)
                (rust rust-mode)
                (scala scala-mode)
                (shell shell-script-mode)
                (smalltalk smalltalk-mode)
                (sql sql-mode)
                (swift swift-mode)
                (visualbasic visual-basic-mode)
                (xml sgml-mode)))
         (language (language-detection-string
                    (buffer-substring-no-properties (point-min) (point-max))))
         (modes (cdr (assoc language map)))
         (mode (cl-loop for mode in modes
                        when (fboundp mode)
                        return mode)))
    (message (format "%s" language))
    (when (fboundp mode)
      mode)))
(setq shr-external-rendering-functions
      '((pre . eww-tag-pre)))

;;; init.el ends here
