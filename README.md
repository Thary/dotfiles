## Thary's dotfiles

### Applications:

![My bswpm](Screenshots/1.png)
*More screenshots in folder ["Screenshots"](Screenshots)*
desktop

1. [GNU Emacs](.emacs.d) (Text editor)
2. [Zsh](.zshrc) (Shell)
3. [Bspwm](.config/bspwm) (Windows manager)
4. [Sxhkd](.config/sxhkd) (Hotkeys)
5. [Alacritty](.config/alacritty) (Terminal)
6. [Rofi](.config/rofi) (Launcher)
7. [Neofetch](.config/neofetch) (System info)
