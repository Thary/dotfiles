set number
set tabstop=4
set smarttab
set foldcolumn=2

syntax on

"" PLUGINS ""
call plug#begin(expand('~/.config/nvim/plugged'))

Plug 'vim-airline/vim-airline'
Plug 'ryanoasis/vim-devicons'

Plug 'preservim/nerdtree'

Plug 'nvim-telescope/telescope.nvim'

" PROGRAMMING "
Plug 'neoclide/coc.nvim'
Plug 'tpope/vim-commentary'

" THEMES "
Plug 'morhetz/gruvbox'
Plug 'sainnhe/everforest'
Plug 'arcticicestudio/nord-vim'

" WEB "
Plug 'ap/vim-css-color'

call plug#end()

set background=dark
colorscheme nord
" colorscheme gruvbox
" colorscheme everforest

set mouse=a
set encoding=UTF-8


let NERDTreeShowHidden = 1


map <C-n> :NERDTreeToggle<CR>
map <F8> :TagbarToggle<CR>

" Tab navigation
map <C-t> :tabnew<CR>
map <C-w> :tabclose<CR>
map <C-l> :tabnext<CR>
map <C-h> :tabprevious<CR>
map <C-q> :q<CR>

" Autoclose
" imap {     {}<Left>
" imap [     []<Left>
" imap (     ()<Left>
" imap '     ''<Left>
" imap "     ""<Left>
" imap <     <><Left>

" DO NOT USE ARROWS
nmap <Left> :echoe "Use h"<CR>
nmap <Right> :echoe "Use l"<CR>
nmap <Up> :echoe "Use k"<CR>
nmap <Down> :echoe "Use j"<CR>
vmap <Left> :echoe "Use h"<CR>
vmap <Right> :echoe "Use l"<CR>
vmap <Up> :echoe "Use k"<CR>
vmap <Down> :echoe "Use j"<CR>

" Normal mod
imap jj <ESC>
