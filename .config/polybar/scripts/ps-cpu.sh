#!bin/bash

 notify-send "Sort by CPU" "$(ps -o comm,pid --sort pcpu -u $USER | grep -v -e "init" -e "\[" -e "\/" | sed -n '1!p' | awk 'NR<11{print}')"
